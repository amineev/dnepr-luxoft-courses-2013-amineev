package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 25.04.13
 * Time: 21:07
 * To change this template use File | Settings | File Templates.
 */

public class WalletTest {
    @Before
    public void start() {
        User vanyaIvanov = new User((long) 2, "Ivan Ivanov", new Wallet((long) 2, BigDecimal.valueOf(2500.50), WalletStatus.ACTIVE, BigDecimal.valueOf(100000.00)));
    }

    @Test(expected = WalletIsBlockedException.class)
    public void testCheckWithdrawalWalletIsBlockedException() throws Exception {

        User alexIvanov = new User((long) 1, "Alexey Ivanov", new Wallet((long) 1, BigDecimal.valueOf(1250.30), WalletStatus.BLOCKED, BigDecimal.valueOf(100000.00)));

        alexIvanov.getWallet().checkWithdrawal(BigDecimal.valueOf(50.50));


    }

    @Test(expected = InsufficientWalletAmountException.class)
    public void testCheckWithdrawalInsufficientWalletAmountException() throws Exception {

        User alexIvanov = new User((long) 1, "Alexey Ivanov", new Wallet((long) 1, BigDecimal.valueOf(1250.30), WalletStatus.ACTIVE, BigDecimal.valueOf(100000.00)));
        alexIvanov.getWallet().checkWithdrawal(BigDecimal.valueOf(5000.50));


    }

    @Test
    public void testWithdrawal() throws Exception {

        User alexIvanov = new User((long) 1, "Alexey Ivanov", new Wallet((long) 1, BigDecimal.valueOf(1250.30), WalletStatus.ACTIVE, BigDecimal.valueOf(100000.00)));
        alexIvanov.getWallet().withdraw(BigDecimal.valueOf(250.30));
        assertEquals(BigDecimal.valueOf(1000.0), alexIvanov.getWallet().getAmount());


    }

    @Test(expected = WalletIsBlockedException.class)
    public void checkTransferWalletIsBlockedException() throws Exception {

        User alexIvanov = new User((long) 1, "Alexey Ivanov", new Wallet((long) 1, BigDecimal.valueOf(1250.30), WalletStatus.BLOCKED, BigDecimal.valueOf(100000.00)));

        alexIvanov.getWallet().checkTransfer(BigDecimal.valueOf(50.50));


    }

    @Test(expected = LimitExceededException.class)
    public void checkTransferLimitExceededException() throws Exception {

        User alexIvanov = new User((long) 1, "Alexey Ivanov", new Wallet((long) 1, BigDecimal.valueOf(1250.30), WalletStatus.ACTIVE, BigDecimal.valueOf(100000.00)));
        alexIvanov.getWallet().checkTransfer(BigDecimal.valueOf(100003));


    }

    @Test
    public void testTransfer() throws Exception {
        User alexIvanov = new User((long) 1, "Alexey Ivanov", new Wallet((long) 1, BigDecimal.valueOf(1250.30), WalletStatus.ACTIVE, BigDecimal.valueOf(100000.00)));
        alexIvanov.getWallet().transfer(BigDecimal.valueOf(1000));
        assertEquals(BigDecimal.valueOf(2250.30), alexIvanov.getWallet().getAmount());
    }
}
