package com.luxoft.dnepr.courses.regular.unit7_1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 17.05.13
 * Time: 16:24
 * To change this template use File | Settings | File Templates.
 */
public class ThreadProducerTest {
    @Test
    public void testGetNewThread() throws Exception {
        Thread tmpThread;
        tmpThread = ThreadProducer.getNewThread();
        assertEquals(tmpThread.getState(), Thread.State.NEW);
        System.out.println(tmpThread.getState());
    }


    @Test
    public void testGetRunnableThread() throws Exception {
        Thread tmpThread;
        tmpThread = ThreadProducer.getRunnableThread();
        assertEquals(tmpThread.getState(), Thread.State.RUNNABLE);
        System.out.println(tmpThread.getState());
    }

    @Test
    public void testGetBlockedThread() throws Exception {
        Thread tmpThread;
        tmpThread = ThreadProducer.getBlockedThread();
        assertEquals(tmpThread.getState(), Thread.State.BLOCKED);
        System.out.println(tmpThread.getState());
    }

    @Test
    public void testGetWaitingThread() throws Exception {
        Thread tmpThread;
        tmpThread = ThreadProducer.getWaitingThread();
        assertEquals(tmpThread.getState(), Thread.State.WAITING);
        System.out.println(tmpThread.getState());

    }

    @Test
    public void testGetTimedWaitingThread() throws Exception {
        Thread tmpThread;
        tmpThread = ThreadProducer.getTimedWaitingThread();
        assertEquals(tmpThread.getState(), Thread.State.TIMED_WAITING);
        System.out.println(tmpThread.getState());
    }

    @Test
    public void testGetTerminatedThread() throws Exception {
        Thread tmpThread;
        tmpThread = ThreadProducer.getTerminatedThread();
        assertEquals(tmpThread.getState(), Thread.State.TERMINATED);
        System.out.println(tmpThread.getState());
    }
}
