package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import java.util.GregorianCalendar;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;


public class BookTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book) book.clone();
        assertEquals(book.getName(), cloned.getName());
        assertEquals(book.getDate(), cloned.getDate());
        assertEquals(book.getCode(), cloned.getCode());
        assertEquals(book.hashCode(), cloned.hashCode());
        assertNotSame(book, cloned);
    }

    @Test
    public void testEquals() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book bookSame = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        assertEquals(true, bookSame.equals(book));
        assertEquals(true, bookSame.getCode().equals(book.getCode()));
        assertEquals(true, bookSame.getName().equals(book.getName()));


    }
}
