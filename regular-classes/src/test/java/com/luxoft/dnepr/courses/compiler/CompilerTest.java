package com.luxoft.dnepr.courses.compiler;

import org.junit.Test;

public class CompilerTest extends AbstractCompilerTest {

    @Test
    public void testSimple() {
        assertCompiled(4, "2+2");
        assertCompiled(5, " 2 + 3 ");
        assertCompiled(1, "2 - 1 ");
        assertCompiled(6, " 2 * 3 ");
        assertCompiled(4.5, "  9 /2 ");

        assertCompiled(0, "3/0");

        assertCompiled(0, "a + b");
        assertCompiled(0, "");
        assertCompiled(0, "a  x c");
        assertCompiled(0, "a  = c");
        assertCompiled(0, "a +- c");
        assertCompiled(0, "^%$  = c");
        assertCompiled(0, "a +-* c");
        assertCompiled(0, "    ");
        assertCompiled(0, "*-+");
    }

    @Test
    public void testComplex() {
        assertCompiled(12, "  (2 + 2 ) * 3 ");
        assertCompiled(8.5, "  2.5 + 2 * 3 ");
        assertCompiled(8.5, "  2 *3 + 2.5");
    }
}
