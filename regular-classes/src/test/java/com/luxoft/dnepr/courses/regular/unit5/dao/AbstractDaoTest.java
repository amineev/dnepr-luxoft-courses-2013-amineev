package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 06.05.13
 * Time: 19:13
 * To change this template use File | Settings | File Templates.
 */
public class AbstractDaoTest {
    private EmployeeDaoImpl a;

    @Before
    public void createBase() throws Exception {
        a = new EmployeeDaoImpl();
        try {
            a.save(new Employee(Long.valueOf(1), 2500));
            a.save(new Employee(Long.valueOf(3), 2500));
            a.save(new Redis(Long.valueOf(4), 100));
            a.save(new Redis(Long.valueOf(9), 300));
            a.save(new Employee(Long.valueOf(11), 3000));

        } catch (UserAlreadyExist userAlreadyExist) {
        }
    }

    @Test
    public void testSave() throws Exception {

        Redis entSv = (Redis) a.get(Long.valueOf(4));
        assertEquals(entSv.getId(), Long.valueOf(4));
        assertEquals(entSv.getWeight(), 100);
        Employee entSvE = (Employee) a.get(Long.valueOf(1));
        assertEquals(entSvE.getId(), Long.valueOf(1));
        assertEquals(entSvE.getSalary(), 2500);

    }

    @Test
    public void testUpdate() throws Exception {
        Redis red = new Redis(Long.valueOf(9), 200);
        try {
            a.update(red);
        } catch (UserNotFound userNotFound) {
        }
        Redis redSame = (Redis) a.get(Long.valueOf(9));
        Long r = redSame.getId();
        assertEquals(r, Long.valueOf(9));
        assertEquals(redSame.getWeight(), 200);

    }

    @Test
    public void testDelete() throws Exception {
        assertEquals(true, a.delete(Long.valueOf(4)));
        assertEquals(false, a.delete(Long.valueOf(5)));
        assertEquals(false, a.delete(Long.valueOf(4)));
    }

    @Test
    public void testGet() throws Exception {
        Redis ent = (Redis) a.get(Long.valueOf(4));
        assertEquals(ent.getId(), Long.valueOf(4));
        assertEquals(ent.getWeight(), 100);
        Redis entTmp = (Redis) a.get(Long.valueOf(23));
        assertEquals(entTmp, null);
    }

    @Test(expected = UserAlreadyExist.class)
    public void testSaveExeptionUAE() throws Exception {
        Entity emp = new Employee(Long.valueOf(1), 2500);
        a.save(emp);
    }

    @Test(expected = UserNotFound.class)
    public void testUpdateExeptionUNF() throws Exception {
        Entity emp = new Employee(Long.valueOf(78), 2500);
        a.update(emp);
        Entity red = new Redis(Long.valueOf(43), 2500);
        a.update(red);
    }
}

