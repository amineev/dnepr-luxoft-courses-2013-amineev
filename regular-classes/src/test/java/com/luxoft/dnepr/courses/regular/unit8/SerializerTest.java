package com.luxoft.dnepr.courses.regular.unit8;

import org.junit.Test;

import java.io.File;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 22.05.13
 * Time: 17:46
 * To change this template use File | Settings | File Templates.
 */
public class SerializerTest {

    @Test
    public void testSerialize() throws Exception {
        Person person = new Person();
        person.setBirthDate(new Date());
        person.setEthnicity("ukrainian");
        person.setGender(Gender.FEMALE);
        person.setName("Galja");
        Person father = new Person();
        father.setBirthDate(new Date());
        father.setEthnicity("ukrainian");
        father.setGender(Gender.MALE);
        father.setName("Vasiliy");
        person.setFather(father);
        FamilyTree obj = new FamilyTree(person);
        File files = new File("D:\\1.json");
        Serializer.serialize(files, obj);


    }

    @Test
    public void testDeserialize() throws Exception {
        Person person = new Person();
        person.setBirthDate(new Date());
        person.setEthnicity("ukrainian");
        person.setGender(Gender.FEMALE);
        person.setName("Galja");
        Person father = new Person();
        father.setBirthDate(new Date());
        father.setEthnicity("ukrainian");
        father.setGender(Gender.MALE);
        father.setName("Vasiliy");
        person.setFather(father);
        FamilyTree obj = new FamilyTree(person);
        File files = new File("D:\\1.json");
        Serializer.serialize(files, obj);

        File filed = new File("D:\\1.json");
        FamilyTree objd = Serializer.deserialize(filed);
        assertEquals(true, objd.equals(obj));
    }
}
