package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 21.04.13
 * Time: 21:09
 * To change this template use File | Settings | File Templates.
 */
public class BeverageTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testEquals() throws Exception {
        Beverage tmpBeverage = productFactory.createBeverage("code", "VODKA", 20, true);
        Beverage tmpSameBeverage = productFactory.createBeverage("code", "VODKA", 20, true);
        assertEquals(true, tmpSameBeverage.equals(tmpBeverage));

        assertEquals(tmpSameBeverage.hashCode(), tmpBeverage.hashCode());
        Beverage tmpClonedBeverage = (Beverage) tmpBeverage.clone();
        assertEquals(true, tmpClonedBeverage.equals(tmpBeverage));
        assertEquals(tmpClonedBeverage.hashCode(), tmpBeverage.hashCode());
        assertNotSame(tmpClonedBeverage, tmpBeverage);
        assertEquals(true, tmpSameBeverage.isNonAlcoholic());
        assertEquals(true, tmpClonedBeverage.isNonAlcoholic());
        assertEquals(true, tmpBeverage.isNonAlcoholic());
    }
}
