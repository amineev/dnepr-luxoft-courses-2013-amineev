package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 21.04.13
 * Time: 21:17
 * To change this template use File | Settings | File Templates.
 */
import static org.junit.Assert.*;

public class BreadTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Bread bread = productFactory.createBread("bread1", "White fresh bread", 10, 1.5);
        Bread cloned = (Bread) bread.clone();
        assertEquals(bread.getName(), cloned.getName());
        assertEquals(bread.getWeight(), cloned.getWeight(), 0.00001);
        assertEquals(bread.getCode(), cloned.getCode());
        assertEquals(bread.hashCode(), cloned.hashCode());
        assertNotSame(bread, cloned);
    }


    @Test
    public void testEquals() throws Exception {
        Bread bread1 = productFactory.createBread("101", "White ", 100, 1.5);
        Bread bread2 = productFactory.createBread("101", "White ", 100, 1.5);

        assertEquals(bread1.getName(), bread2.getName());
        assertEquals(bread1.getWeight(), bread2.getWeight(), 0.00001);
        assertEquals(bread1.getCode(), bread2.getCode());
        assertEquals(bread1.hashCode(), bread2.hashCode());

        if (bread1.equals(bread2)) System.out.println("test pass");
        assertNotSame(bread1, bread2);
    }
}