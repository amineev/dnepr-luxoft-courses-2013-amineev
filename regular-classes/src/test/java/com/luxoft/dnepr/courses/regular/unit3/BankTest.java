package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 25.04.13
 * Time: 23:23
 * To change this template use File | Settings | File Templates.
 */
public class BankTest {

    public static final String JAVA_VERSION = "1.7.0_17";

    @Test
    public void testMakeMoneyTransaction() throws Exception {
        Map<Long, UserInterface> users = new HashMap<Long, UserInterface>();
        users.put((long) 1, new User((long) 1, "Alexey Ivanov", new Wallet((long) 1, BigDecimal.valueOf(1250.3), WalletStatus.ACTIVE, BigDecimal.valueOf(100000.00))));
        users.put((long) 2, new User((long) 2, "Ivan Alexeev", new Wallet((long) 2, BigDecimal.valueOf(5250.3), WalletStatus.ACTIVE, BigDecimal.valueOf(100000.00))));
        users.put((long) 3, new User((long) 3, "Ali Muchamed", new Wallet((long) 3, BigDecimal.valueOf(134250.3), WalletStatus.ACTIVE, BigDecimal.valueOf(100000.00))));

        Bank jpMorgan = new Bank(JAVA_VERSION);
        jpMorgan.setUsers(users);
        jpMorgan.makeMoneyTransaction((long) 1, (long) 2, BigDecimal.valueOf(500.1));
        assertEquals(BigDecimal.valueOf(750.2), jpMorgan.getUsers().get((long) 1).getWallet().getAmount());

        assertEquals(BigDecimal.valueOf(5750.4), jpMorgan.getUsers().get((long) 2).getWallet().getAmount());

    }


    @Test(expected = IllegalJavaVersionError.class)
    public void testBank() throws Exception {
        Bank myBank = new Bank("1.7");
    }

    @Test(expected = NoUserFoundException.class)
    public void testFindUserById() throws NoUserFoundException, TransactionException {
        Map<Long, UserInterface> users = new HashMap<Long, UserInterface>();
        users.put((long) 1, new User((long) 1, "test user 1", new Wallet((long) 1, BigDecimal.valueOf(1234.50), WalletStatus.ACTIVE, BigDecimal.valueOf(100000.00))));
        users.put((long) 2, new User((long) 2, "test user 2", new Wallet((long) 1, BigDecimal.valueOf(1234.50), WalletStatus.ACTIVE, BigDecimal.valueOf(100000.00))));

        Bank myBank = new Bank(JAVA_VERSION);
        myBank.setUsers(users);
        myBank.makeMoneyTransaction((long) 1, (long) 3, BigDecimal.valueOf(1000.0));
    }

}
