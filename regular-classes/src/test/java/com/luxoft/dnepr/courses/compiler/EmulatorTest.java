package com.luxoft.dnepr.courses.compiler;

import static com.luxoft.dnepr.courses.compiler.VirtualMachine.*;

import org.junit.Test;

public class EmulatorTest extends AbstractCompilerTest {

    @Test
    public void testExample1() {
        assertBytecode(5, asm(
                PUSH, 2,
                PUSH, 3,
                ADD,
                PRINT));
    }

    @Test
    public void testExample2() {
        assertBytecode(14, asm(
                PUSH, 3,
                PUSH, 4,
                MUL,
                PUSH, 2,
                ADD,
                PRINT));
        assertBytecode(14, asm(
                PUSH, 2,
                PUSH, 3,
                PUSH, 4,
                MUL,
                ADD,
                PRINT));
    }

    @Test
    public void testExample3() {
        assertBytecode(2.5, asm(
                PUSH, 1,
                PUSH, 4,
                ADD,
                PUSH, 1.5d,
                PUSH, 0.5d,
                ADD,
                SWAP,
                DIV,
                PRINT));
    }

    @Test
    public void testPush() {
        assertBytecode(5, asm(
                PUSH, 5,
                PRINT));

        assertBytecode(5, asm(
                PUSH, 5,
                PRINT));
    }

    @Test
    public void testPop() {
        assertBytecode(2.3, asm(
                PUSH, 2.3,
                PUSH, 3,
                POP,
                PRINT));

        assertBytecode(5.1, asm(
                PUSH, 5.1,
                PUSH, 3,
                POP,
                PRINT));
    }

    @Test
    public void testSwap() {
        assertBytecode(5.1, asm(
                PUSH, 5.1,
                PUSH, 3,
                SWAP,
                PRINT));
        assertBytecode(3.12, asm(
                PUSH, 5,
                PUSH, 3.12,
                SWAP,
                POP,
                PRINT));
    }

    @Test
    public void testDup() {
        assertBytecode(25, asm(
                PUSH, 5,
                DUP,
                MUL,
                PRINT));
        assertBytecode(10.8, asm(
                PUSH, 5.4,
                DUP,
                ADD,
                PRINT));
    }

    @Test
    public void testAdd() {
        assertBytecode(16.15, asm(
                PUSH, 5.65,
                PUSH, 10.5,
                ADD,
                PRINT));

        assertBytecode(426.16, asm(
                PUSH, 15.65,
                PUSH, 410.51,
                ADD,
                PRINT));
    }

    @Test
    public void testMul() {
        assertBytecode(50.8625, asm(
                PUSH, 15.65,
                PUSH, 3.25,
                MUL,
                PRINT));
    }

    @Test
    public void testSub() {
        assertBytecode(-12.39, asm(
                PUSH, 15.65,
                PUSH, 3.26,
                SUB,
                PRINT));
    }

    @Test
    public void testDiv() {
        assertBytecode(-12.39, asm(
                PUSH, 15.65,
                PUSH, 3.26,
                SUB,
                PRINT));
    }
}
