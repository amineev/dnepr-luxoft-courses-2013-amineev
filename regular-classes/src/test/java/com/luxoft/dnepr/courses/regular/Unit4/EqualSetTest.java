package com.luxoft.dnepr.courses.regular.unit4;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 28.04.13
 * Time: 19:58
 * To change this template use File | Settings | File Templates.
 */
public class EqualSetTest {

    @Test
    public void testSize() throws Exception {
        EqualSet<String> tmp = new EqualSet<String>();
        tmp.add("1");
        tmp.add("2");
        assertEquals(2, tmp.size());
        tmp.add(null);
        assertEquals(3, tmp.size());
        tmp.add(null);
        assertEquals(3, tmp.size());
        tmp.remove("2");
        assertEquals(2, tmp.size());
        tmp.add("3");
        assertEquals(tmp.size(), tmp.toArray().length);
        tmp.remove(null);
        assertEquals(2, tmp.size());
        EqualSet<String> tmpSecond = new EqualSet<String>(tmp);
        tmpSecond.remove("1");
        tmp.removeAll(tmpSecond);
        assertEquals(1, tmp.size());
        tmp.clear();
        assertEquals(0, tmp.size());
        tmp.add("1");
        tmp.add("2");
        tmp.add("3");
        tmp.add("4");
        tmp.add("5");
        tmp.add(null);
        assertEquals(true, tmp.contains(null));
        assertEquals(false, tmp.contains("456"));
        tmpSecond.clear();
        tmpSecond = new EqualSet<String>(tmp);
        tmpSecond.remove(null);
        assertEquals(true, tmp.containsAll(tmpSecond));
        tmpSecond.remove("5");
        assertEquals(true, tmp.containsAll(tmpSecond));
        assertEquals(false, tmpSecond.containsAll(tmp));
        tmpSecond.remove("5");
        tmpSecond.remove(null);
        assertEquals(true, tmp.containsAll(tmpSecond));
    }
}
