package com.luxoft.dnepr.courses.regular.unit18.model;

import java.io.*;
import java.util.HashSet;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Vocabulary {
    public HashSet<String> voc = new HashSet<String>();

    private String FromFileToString(String path) throws FileNotFoundException {
        StringBuilder sb = new StringBuilder();
        Scanner inputScanner = new Scanner(new File(path));

        while (inputScanner.hasNext()) {
            sb.append(inputScanner.nextLine());
            sb.append(" ");
        }
        String s = " ";
        return sb.toString();
    }

    private HashSet<String> FromStringToHashSet(String s, Integer wordMinLength) {
         HashSet<String> set = new HashSet<String>();
        StringTokenizer tok = new StringTokenizer(s.toString());
        while (tok.hasMoreTokens()) {
            String to = tok.nextToken();
            if (to.length() > wordMinLength) set.add(to.toLowerCase());
        }
       return set;
    }
         public Vocabulary(String path) throws IOException {
            voc = FromStringToHashSet(FromFileToString(path), Constants.MIN_LENGTH_OF_WORD);
          }
         public String random() {
            return voc.toArray(new String[]{})[(int) (Math.random() * voc.size())];
    }
}
