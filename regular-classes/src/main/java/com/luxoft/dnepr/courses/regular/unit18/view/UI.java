package com.luxoft.dnepr.courses.regular.unit18.view;

import com.luxoft.dnepr.courses.regular.unit18.model.Constants;
import com.luxoft.dnepr.courses.regular.unit18.model.Vocabulary;
import com.luxoft.dnepr.courses.regular.unit18.model.Words;

import java.io.IOException;
import java.util.Scanner;


public class UI implements Runnable {

    private String typerResult(Vocabulary vocab) {
        String w = vocab.random();
        System.out.println(Constants.QUESTION_MESSAGE);
        System.out.println(w);
        return w;
    }

    public void run() {
        String w, st;
        Vocabulary vocab = null;
        try {
            vocab = new Vocabulary(Constants.PATH_TO_TEXT);
        } catch (IOException e) {
        }
        Scanner s = new Scanner(System.in);
        for (; ; ) {
            w = typerResult(vocab);

            // get a boolean here, null for errors

            st = s.next();
            if (st != null) {
                if (st.isEmpty() || st.equals(Constants.COMMANG_EXIT)) {
                    Words.res();
                    return;
                }
                if (Constants.MANAGER_DATA_BASE.containsKey(st)) {
                    System.out.println(Constants.MANAGER_DATA_BASE.get(st));
                    return;
                }
                if (st.equalsIgnoreCase(Constants.COMMANG_YES)) Words.ans(w);
            }
        }
    }

    public static void main(String[] args) {
        UI ui = new UI();
        ui.run();
    }
}
