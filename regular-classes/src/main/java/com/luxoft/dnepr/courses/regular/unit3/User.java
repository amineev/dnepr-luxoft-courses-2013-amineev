package com.luxoft.dnepr.courses.regular.unit3;


public class User implements UserInterface {

    private Long id;
    private String name;
    private WalletInterface wallet;

    public User(Long tmpId, String tmpName, WalletInterface tmpWallet) {
        id = tmpId;
        name = tmpName;
        wallet = tmpWallet;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WalletInterface getWallet() {
        return wallet;
    }

    public void setWallet(WalletInterface wallet) {
        this.wallet = wallet;
    }

}
