package com.luxoft.dnepr.courses.regular.unit6.dao;

import com.luxoft.dnepr.courses.regular.unit6.exception.EntityAlreadyExistException;
import com.luxoft.dnepr.courses.regular.unit6.exception.EntityNotFoundException;

import com.luxoft.dnepr.courses.regular.unit6.model.Entity;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 06.05.13
 * Time: 15:00
 * To change this template use File | Settings | File Templates.
 */
public interface IDao<E extends Entity> {

    E save(E e) throws EntityAlreadyExistException;

    E update(E e) throws EntityNotFoundException;

    E get(Long id);

    boolean delete(Long id);
}