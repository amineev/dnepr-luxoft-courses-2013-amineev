package com.luxoft.dnepr.courses.regular.unit2;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 21.04.13
 * Time: 14:46
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractProduct implements Product, Cloneable {
    private String code;
    private String name;
    private double price;


    public String getCode() {
        return code;
    }


    public String getName() {
        return this.name;
    }


    public double getPrice() {
        return price;
    }

    public void setCode(String tmp) {
        code = tmp;
    }

    public void setName(String tmp) {
        name = tmp;
    }

    public void setPrice(double tmp) {
        price = tmp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractProduct that = (AbstractProduct) o;

        if (!code.equals(that.code)) return false;
        if (!name.equals(that.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = code.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {

        AbstractProduct tmp = (AbstractProduct) super.clone();

        return tmp;
    }


}

