package com.luxoft.dnepr.courses.regular.unit6.model;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 06.05.13
 * Time: 14:59
 * To change this template use File | Settings | File Templates.
 */
public class Redis extends Entity {
    private int weight;


    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Redis(Long id, int weight) {
        this.setId(id);
        this.weight = weight;
    }


}
