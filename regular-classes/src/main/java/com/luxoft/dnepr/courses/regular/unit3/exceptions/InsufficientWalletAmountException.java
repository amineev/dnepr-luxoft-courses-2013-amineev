package com.luxoft.dnepr.courses.regular.unit3.exceptions;


import java.math.BigDecimal;

public class InsufficientWalletAmountException extends Exception {

    private Long walletId;
    private BigDecimal amountToWithdraw;
    private BigDecimal amountInWallet;
    private String message;

    public InsufficientWalletAmountException(Long walletId, BigDecimal amountToWithdraw, BigDecimal amountInWallet, String message) {
        this.walletId = walletId;
        this.amountInWallet = amountInWallet;
        this.amountToWithdraw = amountToWithdraw;
        this.message = message;
    }


    @Override
    public String toString() {
        return message;
    }


    public Long getWalletId() {
        return walletId;
    }


    public BigDecimal getAmountToWithdraw() {
        return amountToWithdraw;
    }


    public BigDecimal getAmountInWallet() {
        return amountInWallet;
    }
}
