package com.luxoft.dnepr.courses.regular.unit2;

import java.util.*;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {

    private List<CompositeProduct> CompositeProducts = new ArrayList<CompositeProduct>();


    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     *
     * @param product new product
     */
    public void append(Product product) {

        for (CompositeProduct i : CompositeProducts) {
            if (product.equals(i.getFirstProduct())) {
                i.add(product);
                return;

            }
        }
        CompositeProduct tmpComp = new CompositeProduct();
        tmpComp.add(product);
        CompositeProducts.add(tmpComp);

    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     *
     * @return
     */
    public double summarize() {
        double totalPrice = 0;
        for (CompositeProduct i : CompositeProducts) {
            totalPrice += i.getPrice();
        }
        return totalPrice;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     *
     * @return
     */


    public List<Product> getProducts() {
        List<Product> sortedListPoduct = new ArrayList<Product>();
        List<CompositeProduct> sortCompositeProducts = this.getCompositeProductList();
        for (CompositeProduct i : sortCompositeProducts) {

            Product tmp = (Product) i;
            sortedListPoduct.add(tmp);

        }


        return sortedListPoduct;
    }


    public class CompositeProductComparable implements Comparator<CompositeProduct> {

        @Override
        public int compare(CompositeProduct p1, CompositeProduct p2) {
            return (p1.getPrice() > p2.getPrice() ? -1 : (p1.getPrice() == p2.getPrice() ? 0 : 1));
        }
    }

    public List<CompositeProduct> getCompositeProductList() {
        Collections.sort(CompositeProducts, new CompositeProductComparable());
        return CompositeProducts;
    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<String>();
        for (CompositeProduct i : CompositeProducts) {
            productInfos.add(i.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize();
    }

}
