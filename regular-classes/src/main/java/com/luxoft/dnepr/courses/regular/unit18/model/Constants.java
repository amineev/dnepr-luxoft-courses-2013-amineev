package com.luxoft.dnepr.courses.regular.unit18.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 29.06.13
 * Time: 20:07
 * To change this template use File | Settings | File Templates.
 */
public class Constants {
    public final static String PATH_TO_TEXT = "D:\\sonnets.txt";
    public final static String ERROR_MESSAGE = "Error while reading answer";
    public final static String HELP_MESSAGE = "Lingustic analizator v1, autor Tushar Brahmacobalol " +
            "for help type help, answer y/n question, your english knowlege will give you";
    public final static String QUESTION_MESSAGE = "Do you know translation of this word?:";

    public final static String COMMAND_HELP = "help";
    public final static String COMMANG_EXIT = "exit";
    public final  static String COMMANG_YES = "Y";
    public final static int MIN_LENGTH_OF_WORD = 3;
    public  static Map<String, String> MANAGER_DATA_BASE=null;
       static {
        Map<String, String> result = new HashMap<String, String>();
        result.put(COMMAND_HELP, HELP_MESSAGE);

        MANAGER_DATA_BASE = Collections.unmodifiableMap(result);
    }


}
