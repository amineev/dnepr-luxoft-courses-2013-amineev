package com.luxoft.dnepr.courses.compiler;

/**
 * Interface for virtual machine to run instructions.
 * All instructions are listed below:
 * <p/>
 * PUSH pushes next value to stack
 * POP pops value from stack
 * SWAP exchanges last two values
 * DUB dublicates the top of the stack
 * <p/>
 * ADD sums two last values in stack and pushes result in stack
 * MUL multiplies two last values in stack and pushes result in stack
 * SUB subtracts last pre-value in stack from the last and pushes result in stack
 * DIV divides last value in stack on the pre-last and pushes result in stack
 * <p/>
 * PRINT prints the top of the stack
 * <p/>
 * Example 1:
 * we need to calculate expression:
 * 2 + 3
 * for this we need to execute following code:
 * PUSH 2     # stack contains 2
 * PUSH 3     # stack contains 2 | 3
 * ADD        # sums last two pushed values to 5 and pushes it to the stack : 5
 * PRINT      # prints the top of the stack.
 * <p/>
 * Example 2:
 * we need to compile next expression:
 * 2 + 3 * 4
 * running this code should give us 14 (as multiplication has higher priority).
 * One way to do so is following bytecode:
 * <p/>
 * PUSH 3    # pushes 3 to stack: 3
 * PUSH 4    # pushes 4 to stack: 3 | 4
 * MUL       # multiplies two last values: 12
 * PUSH 2    # pushes 2: 12 | 2
 * ADD       # add two last values: 14
 * PRINT     # prints the top
 * <p/>
 * Other way to do the same:
 * PUSH 2    # 2
 * PUSH 3    # 2 | 3
 * PUSH 4    # 2 | 3 | 4
 * MUL       # 2 | 12
 * ADD       # 14
 * PRINT
 * <p/>
 * Example 3:
 * Expression is:
 * (1 + 4) / (1.5 + 0.5)
 * priority here is specified by brackets (result is 2.5).
 * <p/>
 * Bytecode can be following:
 * PUSH 1    # 1
 * PUSH 4    # 1 | 4
 * ADD       # 5
 * PUSH 1.5  # 5 | 1.5
 * PUSH 0.5  # 5 | 1.5 | 0.5
 * ADD       # 5 | 2
 * # Note that values are not in order: if we use DIV now it will result 2/5
 * SWAP      # 5 | 2
 * DIV       # 2.5
 * PRINT     # prints result
 *
 * @author OBereznevatyy
 */
public interface VirtualMachine extends Runnable {
    /**
     * Pushes its argument in the stack:
     * A
     * # PUSH B
     * A B
     */
    byte PUSH = 0x01;

    /**
     * Pops (removes) the top of the stack:
     * A B
     * # POP
     * A
     */
    byte POP = 0x02;

    /**
     * Exchanges two last values in the stack:
     * A B
     * # SWAP
     * B A
     */
    byte SWAP = 0x03;

    /**
     * Dublicates the top of the stack:
     * A B
     * # DUP
     * A B B
     */
    byte DUP = 0x04;

    /**
     * Adds (sums) two last values in the stack and pushes result in the stack:
     * A B
     * # ADD
     * C[=A+B]
     */
    byte ADD = 0x05;

    /**
     * Multiplies two last values in the stack and pushes result in the stack:
     * A B
     * # MUL
     * C[=A*B]
     */
    byte MUL = 0x06;

    /**
     * Subtracts pre-last value in the stack from last one and pushes result in the stack:
     * A B
     * # SUB
     * C[=B-A]
     */
    byte SUB = 0x07;

    /**
     * Divides last value in the stack on pre-last one and pushes result in the stack:
     * A B
     * # SUB
     * C[=B/A]
     */
    byte DIV = 0x08;

    /**
     * Prints the top of the stack to the console:
     * A B
     * # PRINT
     * A
     */
    byte PRINT = 0x09;
}
