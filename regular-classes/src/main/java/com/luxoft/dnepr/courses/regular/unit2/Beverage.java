package com.luxoft.dnepr.courses.regular.unit2;

public class Beverage extends AbstractProduct implements Product {

    private boolean nonalcoholic;

    public boolean isNonAlcoholic() {
        return nonalcoholic;
    }

    void setNonAlcoholic(boolean tempSetNonAlco) {
        this.nonalcoholic = tempSetNonAlco;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Beverage beverage = (Beverage) o;

        if (nonalcoholic != beverage.nonalcoholic) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (nonalcoholic ? 1 : 0);
        return result;
    }
}


