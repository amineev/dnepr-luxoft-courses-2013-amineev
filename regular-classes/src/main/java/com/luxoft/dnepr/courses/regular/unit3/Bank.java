package com.luxoft.dnepr.courses.regular.unit3;


import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public  class Bank implements BankInterface {

    private Map<Long, UserInterface> users;

    public Bank(String expectedJavaVersion) throws IllegalJavaVersionError {
        if (!System.getProperty("java.version").equals(expectedJavaVersion)) {
            throw new IllegalJavaVersionError(System.getProperty("java.version"), expectedJavaVersion, "");
        }
    }

    public Map<Long, UserInterface> getUsers() {
        return Collections.unmodifiableMap(users);
    }

    public void setUsers(Map<Long, UserInterface> users) {
        this.users = new HashMap<Long, UserInterface>(users);
    }

    private UserInterface findUser(Map<Long, UserInterface> tmpMap, long UserId) throws NoUserFoundException {

        for (Map.Entry<Long, UserInterface> i : tmpMap.entrySet()) {
            if (i.getValue().getId().equals(UserId)) {
                return i.getValue();
            }

        }
        throw new NoUserFoundException(UserId, "");

    }


    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException, TransactionException {

        UserInterface UIFromUserId = findUser(users, fromUserId);
        UserInterface UIToUserId = findUser(users, toUserId);
        try {

            UIFromUserId.getWallet().checkWithdrawal(amount);

        } catch (WalletIsBlockedException e) {
            throw new TransactionException(WalletIsBlockedMessage(UIFromUserId));
        } catch (InsufficientWalletAmountException e) {
            throw new TransactionException(InsufficientWalletAmountMessage(UIFromUserId, amount));
        }


        try {

            UIToUserId.getWallet().checkTransfer(amount);

        } catch (WalletIsBlockedException e) {
            throw new TransactionException(WalletIsBlockedMessage(UIToUserId));
        } catch (LimitExceededException e) {
            throw new TransactionException(LimitExceededMessage(UIToUserId, amount));
        }


        UIFromUserId.getWallet().setAmount(UIFromUserId.getWallet().getAmount().subtract(amount));
        UIToUserId.getWallet().setAmount(UIToUserId.getWallet().getAmount().add(amount));
    }

    private String WalletIsBlockedMessage(UserInterface user) {
        return "User " + user.getName() + " wallet is blocked";
    }


    private String InsufficientWalletAmountMessage(UserInterface user, BigDecimal amount) {
        return "User " + user.getName() + " has insufficient funds (" + user.getWallet().getAmount().toString() + " < " + amount.toString() + ")";
    }


    private String LimitExceededMessage(UserInterface user, BigDecimal amount) {
        return "User '" + user.getName() + "' wallet limit exceeded (" + user.getWallet().getAmount().toString() + " + " + amount.toString() + " > " + user.getWallet().getMaxAmount().toString() + ")";
    }


}

