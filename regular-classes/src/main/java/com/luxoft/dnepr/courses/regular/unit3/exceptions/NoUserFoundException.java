package com.luxoft.dnepr.courses.regular.unit3.exceptions;


public class NoUserFoundException extends Exception {

    private Long userId;
    private String message;

    public NoUserFoundException(Long userId, String message) {
        if (message.equals("")) this.message = "User " + userId.toString() + " not found";
        else this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }

    public Long getUserId() {
        return userId;
    }
}
