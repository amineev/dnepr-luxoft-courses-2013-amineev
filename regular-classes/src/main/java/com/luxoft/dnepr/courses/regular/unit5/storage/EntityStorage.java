package com.luxoft.dnepr.courses.regular.unit5.storage;


import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 06.05.13
 * Time: 15:09
 * To change this template use File | Settings | File Templates.
 */
public class EntityStorage {
    private static EntityStorage instance = null;

    private final static Map<Long, Entity> entities = new HashMap();

    private EntityStorage() {
    }

    public static EntityStorage getInstance() {
        if (instance == null) {
            instance = new EntityStorage();
        }
        return instance;
    }

    public static Map<Long, Entity> getEntities() {
        return entities;
    }


}