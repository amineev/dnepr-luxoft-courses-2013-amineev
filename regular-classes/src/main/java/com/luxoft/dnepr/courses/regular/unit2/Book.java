package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Date;

public class Book extends AbstractProduct implements Cloneable {

    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Book book = (Book) o;

        if (!date.equals(book.date)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + date.hashCode();
        return result;
    }

    @Override
    public Book clone() throws CloneNotSupportedException {

        Book tmp = (Book) super.clone();
        tmp.setDate((Date) this.date.clone());

        return tmp;
    }

}
