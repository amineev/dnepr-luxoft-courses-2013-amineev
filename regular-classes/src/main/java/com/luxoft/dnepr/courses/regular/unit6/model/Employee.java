package com.luxoft.dnepr.courses.regular.unit6.model;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 06.05.13
 * Time: 14:59
 * To change this template use File | Settings | File Templates.
 */
public class Employee extends Entity {
    private int salary;

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }


    public Employee(Long id, int salary) {
        this.setId(id);
        this.salary = salary;
    }

}
