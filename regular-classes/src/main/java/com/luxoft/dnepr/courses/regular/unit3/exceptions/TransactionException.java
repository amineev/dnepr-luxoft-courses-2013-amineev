package com.luxoft.dnepr.courses.regular.unit3.exceptions;


public class TransactionException extends Exception {

    private String message;

    public TransactionException(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }

}
