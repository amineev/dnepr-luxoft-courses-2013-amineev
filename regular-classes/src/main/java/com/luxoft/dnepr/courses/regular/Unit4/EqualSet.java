package com.luxoft.dnepr.courses.regular.unit4;

import java.util.*;


/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 28.04.13
 * Time: 15:38
 * To change this template use File | Settings | File Templates.
 */
public class EqualSet<E> implements Set<E> {
    private Collection<E> equalSetData;

    public EqualSet() {
        equalSetData = new ArrayList<E>();
    }

    public EqualSet(Collection<? extends E> collection) {
        equalSetData = new ArrayList();
        if (collection != null) {
            for (Iterator itr = collection.iterator(); itr.hasNext(); ) {
                Object obj = itr.next();
                if (!equalSetData.contains(obj)) {
                    equalSetData.add((E) obj);
                }
            }
        }
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return equalSetData.toArray(a);
    }

    @Override
    public Iterator<E> iterator() {
        return equalSetData.iterator();
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        if (collection == null) return false;
        for (Object tmp : collection) {
            if (!equalSetData.contains(tmp)) return false;
        }
        return true;
    }

    @Override
    public void clear() {
        equalSetData.clear();
    }

    @Override
    public boolean add(E element) {
        if (element == null) {
            for (E tmp : equalSetData) {
                if (tmp == null) {
                    return false;
                }
            }
        }
        return equalSetData.add(element);
    }

    @Override
    public boolean isEmpty() {
        return equalSetData.isEmpty();
    }

    @Override
    public int size() {
        return equalSetData.size();
    }

    @Override
    public Object[] toArray() {
        Object[] tmp = new Object[equalSetData.size()];
        int cont = 0;
        for (E i : equalSetData) {
            tmp[cont] = i;
            cont++;
        }
        return tmp;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        if (collection == null) return false;
        if (equalSetData.size() == 0 || collection.size() == 0) return false;

        boolean flag = true;
        Collection<E> tmpMas = new ArrayList<E>();

        for (E tmp : equalSetData) {
            if (collection.contains(tmp)) tmpMas.add(tmp);
            else flag = false;
        }
        equalSetData.clear();
        equalSetData.addAll(tmpMas);
        return flag;
    }

    @Override
    public boolean remove(Object o) {
        for (E i : equalSetData) {
            if ((i == null && o == null) || (i != null && i.equals(o))) {
                return equalSetData.remove(i);
            }
        }
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        if (collection == null)
            return false;
        boolean flag = false;
        for (Object i : collection) {
            if (equalSetData.remove(i)) {
                flag = true;
            }
        }
        return flag;
    }

    @Override
    public boolean contains(Object element) {
        for (E tmp : equalSetData) {
            if (tmp != null && tmp.equals(element)) return true;
            if (element == null && tmp == null) return true;
        }
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return equalSetData.addAll(c);
    }
}