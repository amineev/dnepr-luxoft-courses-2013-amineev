package com.luxoft.dnepr.courses.regular.unit8;

import java.io.*;
import java.util.Date;

public class FamilyTree implements Externalizable {

    private Person root;
    private transient Date creationTime;

    public FamilyTree() {
        setCreationTime(new Date());
    }

    public FamilyTree(Person root) {
        this();
        setRoot(root);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        String str = "{\"root\":" + this.getRoot().WritePersonToString() + "}";
        out.writeUTF(str);
    }

    public Person getRoot() {
        return root;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    private void setRoot(Person root) {
        this.root = root;
    }

    private void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }
}
