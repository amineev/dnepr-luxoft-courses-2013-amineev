package com.luxoft.dnepr.courses.regular.unit2;

public class Bread extends AbstractProduct implements Cloneable {
    //TODO add missing fields and methods
    private double weight;

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Bread bread = (Bread) o;

        if (Double.compare(bread.weight, weight) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        temp = Double.doubleToLongBits(weight);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
/*
    @Override
    public String getCode() {
        throw new UnsupportedOperationException("getCode()");  //TODO
    }

    @Override
    public String getName() {
        throw new UnsupportedOperationException("getName()");  //TODO
    }

    @Override
    public double getPrice() {
        throw new UnsupportedOperationException("getPrice()");  //TODO
    } */
}
