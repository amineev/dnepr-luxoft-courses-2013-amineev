package com.luxoft.dnepr.courses.regular.unit8;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Date;

public class Person implements Externalizable {

    private String name;
    private Gender gender;
    private String ethnicity;
    private Date birthDate;
    private Person father;
    private Person mother;


    // void Person(String tmpStringIn){
    //     StringBuffer  sb=new StringBuffer(tmpStringIn) ;
    //     sb.
    //     this.name=
    // }
    // private cutter(String from)
    public String WritePersonToString() {

        String strOutTmp = "";
        StringBuffer str = new StringBuffer();
        str.append("{\"name\":\"" + this.getName() + "\"");

        if (this.getGender() == null) {
            str.append(",\"gender\":\"null\"");
        } else {
            str.append(",\"gender\":\"" + this.getGender() + "\"");
        }
        str.append(",\"ethnicity\":\"" + this.getEthnicity() + "\"");
        str.append(",\"birthDate\":\"" + this.getBirthDate() + "\"");
        if (!(this.getFather() == null)) {
            str.append(",\"father\":" + this.getFather().WritePersonToString());
        } else {
            str.append(",\"father\":null");
        }
        if (!(this.getMother() == null)) {
            str.append(",\"mother\":" + this.getMother().WritePersonToString());
        } else {
            str.append(",\"mother\":null");
        }
        strOutTmp = str.toString();
        return str + "}";
    }

    public Person ReadPersonFromString(String strInTmp) {
        Person tmpPersonTop = new Person();
        strInTmp.indexOf("\"name\"");
        tmpPersonTop.name = strInTmp.substring(strInTmp.indexOf("\"name\",:") + 1, strInTmp.indexOf("\"", strInTmp.indexOf("\"name\",:") + 1));

        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (birthDate != null ? !birthDate.equals(person.birthDate) : person.birthDate != null) return false;
        if (ethnicity != null ? !ethnicity.equals(person.ethnicity) : person.ethnicity != null) return false;
        if (father != null ? !father.equals(person.father) : person.father != null) return false;
        if (gender != person.gender) return false;
        if (mother != null ? !mother.equals(person.mother) : person.mother != null) return false;
        if (name != null ? !name.equals(person.name) : person.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Person getFather() {
        return father;
    }

    public void setFather(Person father) {
        this.father = father;
    }

    public Person getMother() {
        return mother;
    }

    public void setMother(Person mother) {
        this.mother = mother;
    }
}
