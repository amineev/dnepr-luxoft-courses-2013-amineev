package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 06.05.13
 * Time: 17:20
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractDao<E extends Entity> implements IDao<E> {

    protected long findMaxId(Map<Long, Entity> hashMap) {
        long maxId = 0;
        for (long i : hashMap.keySet()) {
            if (i > maxId) {
                maxId = i;
            }
        }
        return maxId;
    }

    protected Map<Long, Entity> createStorage() {
        EntityStorage entStorage = EntityStorage.getInstance();
        return entStorage.getEntities();
    }


    public E save(E entity) throws UserAlreadyExist {

        Map<Long, Entity> entHM = createStorage();
        if ((entity.getId()) == null) {
            entity.setId(findMaxId(entHM) + 1);
        }
        if (entHM.containsKey(entity.getId())) {
            throw new UserAlreadyExist();
        }
        entHM.put(entity.getId(), entity);

        return entity;
    }

    public E update(E entity) throws UserNotFound {
        Map<Long, Entity> entHM = createStorage();
        if ((!entHM.containsKey(entity.getId())) || ((entity.getId()) == null)) {
            throw new UserNotFound();
        }

        entHM.remove(entity.getId());
        entHM.put(entity.getId(), entity);

        return entity;
    }

    public E get(Long id) {

        Map<Long, Entity> entHM = createStorage();
        if ((!entHM.containsKey(id)) || (id == null)) {
            return null;
        }
        return (E) entHM.get(id);

    }

    public boolean delete(Long id) {
        Map<Long, Entity> entHM = createStorage();
        if (!entHM.containsKey(id)) {
            return false;
        }

        entHM.remove(id);
        return true;


    }
}
