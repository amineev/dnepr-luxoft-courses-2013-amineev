package com.luxoft.dnepr.courses.regular.unit8;

import com.google.gson.Gson;

import java.io.*;
import java.util.Date;

public class Serializer {
    public static void serialize(File file, FamilyTree entity) {
        Gson gson = new Gson();
        String json = gson.toJson(entity);
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(json);
            writer.close();
        } catch (IOException ex) {
        }
    }

    public static FamilyTree deserialize(File file) {
        Gson gson = new Gson();
        FamilyTree obj = null;
        try {
            BufferedReader br = new BufferedReader(
                    new FileReader(file));
            obj = gson.fromJson(br, FamilyTree.class);
        } catch (IOException e) {
        }
        return obj;
    }

    public static void main(String[] args) {

        Person person = new Person();
        person.setBirthDate(new Date());
        person.setEthnicity("ukrainian");
        person.setGender(Gender.FEMALE);
        person.setName("Galja");
        Person father = new Person();
        father.setBirthDate(new Date());
        father.setEthnicity("ukrainian");
        father.setGender(Gender.MALE);
        father.setName("Vasiliy");
        person.setFather(father);
        FamilyTree obj = new FamilyTree(person);
        File files = new File("D:\\1.json");
        Serializer.serialize(files, obj);
        File filed = new File("D:\\2.json");
        try {


            FileWriter writer = new FileWriter(filed);
            writer.write(person.WritePersonToString());
            writer.flush();
            writer.close();
        } catch (IOException e) {
        }


    }
}