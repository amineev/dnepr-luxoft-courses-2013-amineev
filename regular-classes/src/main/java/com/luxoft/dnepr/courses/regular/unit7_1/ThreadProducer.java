package com.luxoft.dnepr.courses.regular.unit7_1;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 17.05.13
 * Time: 14:48
 * To change this template use File | Settings | File Templates.
 */
public final class ThreadProducer {
    public volatile static Integer storage = 5;
    public static boolean flagIsOkBlocked = false;
    public static boolean flagIsOkTerminated = false;
    public static boolean flagIsOkRunning = false;

    private ThreadProducer() {
    }

    public static Thread getNewThread() {
        Thread tmpThread = new Thread();
        return tmpThread;
    }

    public static Thread getRunnableThread() {
        Thread tmpThread = new Thread();
        tmpThread.start();
        return tmpThread;
    }

    public static Thread getBlockedThread() {
        Thread tmpThread = new Thread() {
            public void run() {
                synchronized (storage) {
                    for (; ; ) {
                        storage = 6;
                    }
                }
            }
        };
        Thread killerThread = new Thread() {
            public void run() {
                synchronized (storage) {
                    for (; ; ) {
                        storage = 5;
                        flagIsOkBlocked = true;
                    }
                }
            }
        };
        flagIsOkBlocked = false;
        killerThread.start();
        while (!flagIsOkBlocked) {
        }
        ;
        tmpThread.start();
        while (!(tmpThread.getState() == Thread.State.BLOCKED)) {
        }

        return tmpThread;

    }

    public static Thread getWaitingThread() {
        Thread tmpThread = new Thread() {

            public synchronized void run() {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
        };
        tmpThread.start();

        while (!(tmpThread.getState() == Thread.State.WAITING)) {
        }
        return tmpThread;
    }

    public static Thread getTimedWaitingThread() {
        Thread tmpThread = new Thread() {
            public synchronized void run() {
                try {
                    wait(1000);
                } catch (InterruptedException e) {
                }
            }
        };

        tmpThread.start();
        while (!(tmpThread.getState() == Thread.State.TIMED_WAITING)) {
        }
        return tmpThread;
    }

    public static Thread getTerminatedThread() {
        Thread tmpThread = new Thread() {
            public void run() {
                flagIsOkTerminated = true;
            }
        };
        tmpThread.start();
        while (!flagIsOkTerminated) {
        }
        while (!(tmpThread.getState() == Thread.State.TERMINATED)) {
        }
        return tmpThread;
    }
}