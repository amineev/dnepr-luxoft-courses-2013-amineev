package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Compiler {
    public static final String ERROR_MESSAGE = "ERROR!!!";
    public static final boolean DEBUG = true;

    private static String remakeSpecialSymbols(String symbolType) {
        if (symbolType.trim() == "+") return "\\+";
        else if (symbolType.trim() == "*") return "\\*";
        else return symbolType;
    }

    public static String signFinder(String tmpStr) {
        if (tmpStr.indexOf("*") > 0) return "*";
        else if (tmpStr.indexOf("/") > 0) return "/";
        else if (tmpStr.indexOf("+") > 0) return "+";
        else if (tmpStr.indexOf("-") > 0) return "-";
        return ERROR_MESSAGE;
    }

    public static double[] returnNumbers(String tmpStr) {
        try {
            String[] tmpStrMas = tmpStr.split(remakeSpecialSymbols(signFinder(tmpStr)));
            if (tmpStrMas.length == 2 && tmpStrMas[0].length() > 0 && tmpStrMas[1].length() > 0)
                return new double[]{Double.parseDouble(tmpStrMas[0].trim()), Double.parseDouble(tmpStrMas[1].trim())};
        } catch (Exception e) {
            return new double[]{0, 0};
        }
        return new double[]{0, 0};
    }


    public static void main(String[] args) {
        byte[] byteCode = compile(getInputString(args));
        VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
        vm.run();
    }

    static byte[] compile(String input) {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        String s = signFinder(input);
        if (input.equals(null) || input.equals("") || input.length() < 3 || s.equals(ERROR_MESSAGE)) {
            addCommand(result, VirtualMachine.PUSH, 0);
            addCommand(result, VirtualMachine.PRINT);
            return result.toByteArray();
        }
        double[] mas = returnNumbers(input);

        double firstArg = mas[0];
        double secondArg = mas[1];


        if (s.equals("+")) {

            addCommand(result, VirtualMachine.PUSH, firstArg);
            addCommand(result, VirtualMachine.PUSH, secondArg);
            addCommand(result, VirtualMachine.ADD);
            addCommand(result, VirtualMachine.PRINT);

        }
        if (s.equals("-")) {
            addCommand(result, VirtualMachine.PUSH, secondArg);
            addCommand(result, VirtualMachine.PUSH, firstArg);
            addCommand(result, VirtualMachine.SUB);
            addCommand(result, VirtualMachine.PRINT);

        }
        if (s.equals("*")) {
            addCommand(result, VirtualMachine.PUSH, secondArg);
            addCommand(result, VirtualMachine.PUSH, firstArg);
            addCommand(result, VirtualMachine.MUL);
            addCommand(result, VirtualMachine.PRINT);

        }
        if (s.equals("/")) {
            if (secondArg == 0) {
                firstArg = 0;
                secondArg = 1;
            }

            addCommand(result, VirtualMachine.PUSH, secondArg);

            addCommand(result, VirtualMachine.PUSH, firstArg);
            addCommand(result, VirtualMachine.DIV);
            addCommand(result, VirtualMachine.PRINT);

        }

        // ========================

        return result.toByteArray();
    }

    /**
     * Adds specific command to the byte stream.
     *
     * @param result
     * @param command
     */
    public static void addCommand(ByteArrayOutputStream result, byte command) {
        result.write(command);
    }

    /**
     * Adds specific command with double parameter to the byte stream.
     *
     * @param result
     * @param command
     * @param value
     */
    public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
        result.write(command);
        writeDouble(result, value);
    }

    private static void writeDouble(ByteArrayOutputStream result, double val) {
        long bits = Double.doubleToLongBits(val);

        result.write((byte) (bits >>> 56));
        result.write((byte) (bits >>> 48));
        result.write((byte) (bits >>> 40));
        result.write((byte) (bits >>> 32));
        result.write((byte) (bits >>> 24));
        result.write((byte) (bits >>> 16));
        result.write((byte) (bits >>> 8));
        result.write((byte) (bits >>> 0));
    }

    private static String getInputString(String[] args) {
        if (args.length > 0) {
            return join(Arrays.asList(args));
        }

        Scanner scanner = new Scanner(System.in);
        List<String> data = new ArrayList<String>();
        while (scanner.hasNext()) {
            data.add(scanner.next());
            //break;
        }
        return join(data);
    }


    private static String join(List<String> list) {
        StringBuilder result = new StringBuilder();
        for (String element : list) {
            result.append(element);
        }
        return result.toString();
    }

}
