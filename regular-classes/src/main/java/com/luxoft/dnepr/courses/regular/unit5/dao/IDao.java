package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 06.05.13
 * Time: 15:00
 * To change this template use File | Settings | File Templates.
 */
public interface IDao<E extends Entity> {

    E save(E e) throws UserAlreadyExist;

    E update(E e) throws UserNotFound;

    E get(Long id);

    boolean delete(Long id);
}