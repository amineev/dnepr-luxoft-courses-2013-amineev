package com.luxoft.dnepr.courses.toprank;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.concurrent.*;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 02.06.13
 * Time: 19:29
 * To change this template use File | Settings | File Templates.
 */
public class TopRankSearcher {
    private static final double DEFAULT_DAMPING_FACTOR = 0.8;
    private static final int DEFAULT_NUMBER_OF_LOOPS_IN_RANK_COMPUTING = 10;

    private Map<String, String> urlToStringList(List<String> urls, int numberOfThreads) {
        Map<String, String> urlContent = new ConcurrentHashMap<String, String>();
        final Map<String, String> urlContentFinal = urlContent;
        ExecutorService pull = Executors.newFixedThreadPool(numberOfThreads);
        for (String i : urls) {
            final String tmpIteratorUrl = i;
            pull.submit(new Runnable() {
                public void run() {
                    URLConnection connection = null;
                    URL url = null;
                    try {
                        url = URI.create(tmpIteratorUrl).toURL();
                        connection = url.openConnection();
                        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        StringBuffer sb = new StringBuffer();
                        String tmpString = "";
                        while ((tmpString = br.readLine()) != null) {
                            sb.append(tmpString);
                        }
                        urlContentFinal.put(tmpIteratorUrl, sb.toString());
                    } catch (MalformedURLException e) {
                    } catch (IOException e) {
                    }
                }
            });
        }
        pull.shutdown();
        try {
            pull.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
        }

        return urlContent;
    }

    public TopRankResults execute(List<String> urls) throws IOException, InterruptedException {


        TopRankExecutor executor = new TopRankExecutor(DEFAULT_DAMPING_FACTOR, DEFAULT_NUMBER_OF_LOOPS_IN_RANK_COMPUTING);
        Map<String, String> path = urlToStringList(urls, 3);
        TopRankResults results = executor.execute(path);

        return results;
    }

    public TopRankResults execute(List<String> urls, double dampingFactor, int numberOfLoopsInRankComputing) throws IOException, InterruptedException {
        TopRankExecutor executor = new TopRankExecutor(dampingFactor, numberOfLoopsInRankComputing);
        TopRankResults results = executor.execute(urlToStringList(urls, 3));

        return results;
    }
}
