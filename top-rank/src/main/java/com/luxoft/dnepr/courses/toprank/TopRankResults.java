package com.luxoft.dnepr.courses.toprank;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 17.05.13
 * Time: 15:14
 * To change this template use File | Settings | File Templates.
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TopRankResults {
    private Map<String, List<String>> revgraph = new HashMap();
    private Map<String, List<String>> graph = new HashMap();

    private Map<String, List<String>> index = new HashMap();
    private Map<String, Double> ranks = new HashMap();

    public Map<String, List<String>> getGraph() {
        return graph;
    }

    public Map<String, List<String>> getReverseGraph() {
        return revgraph;
    }

    public Map<String, List<String>> getIndex() {
        return index;
    }

    public Map<String, Double> getRanks() {
        return ranks;
    }

    public void setIndex(Map<String, List<String>> index) {
        this.index = index;
    }

    public void setGraph(Map<String, List<String>> graph) {
        this.graph = graph;
    }

    public void setReverseGraph(Map<String, List<String>> revgraph) {
        this.revgraph = revgraph;
    }

    public void setRanks(Map<String, Double> ranks) {
        this.ranks = ranks;
    }
}
