package com.luxoft.dnepr.courses.toprank;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 17.05.13
 * Time: 15:14
 * To change this template use File | Settings | File Templates.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TopRankExecutor {
    private double dampingFactor;
    private int numberOfLoopsInRankComputing;
    private static final int COUNT_OF_THREADS = 3;


    public TopRankExecutor(double dampingFactor, int numberOfLoopsInRankComputing) {
        this.dampingFactor = dampingFactor;
        this.numberOfLoopsInRankComputing = numberOfLoopsInRankComputing;
    }

    public TopRankResults execute(Map<String, String> urlContent) {
        TopRankResults results = new TopRankResults();

        results.setGraph(getGraph(urlContent, COUNT_OF_THREADS));
        results.setReverseGraph(reverseGraph(urlContent, COUNT_OF_THREADS));
        results.setRanks(getRanks(urlContent, reverseGraph(urlContent, COUNT_OF_THREADS)));
        results.setIndex(getIndex(urlContent, COUNT_OF_THREADS));
        return results;
    }

    private Map<String, List<String>> getIndex(final Map<String, String> urlContent, int threadNumber) {
        final Map<String, List<String>> index = new HashMap();
        ExecutorService pullGraph = Executors.newFixedThreadPool(threadNumber);
        for (final String i : urlContent.keySet()) {
            pullGraph.submit(new Runnable() {
                public void run() {
                    for (String j : urlContent.get(i).split("[\t\n\\s]")) {
                        if (!index.containsKey(j)) {
                            index.put(j, new ArrayList());
                            index.get(j).add(i);
                        }
                        if (!index.get(j).contains(i)) {
                            index.get(j).add(i);
                        }
                    }
                }
            });
        }
        pullGraph.shutdown();
        try {
            pullGraph.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
        }

        return index;
    }

    private Map<String, List<String>> getGraph(final Map<String, String> urlContent, int threadNumber) {
        final Map<String, List<String>> graph = new HashMap<String, List<String>>();


        for (String i : urlContent.keySet()) {
            graph.put(i, new ArrayList());
        }
        ExecutorService pullGraph = Executors.newFixedThreadPool(threadNumber);


        for (final String i : urlContent.keySet()) {
            final String tmpIteratorUrl = i;
            pullGraph.submit(new Runnable() {
                public void run() {
                    List<String> tmpList = new ArrayList();
                    for (String k : graph.keySet()) {
                        if ((urlContent.get(k).contains(i)) && (i != k)) {
                            tmpList.add(k);
                        }
                    }
                    graph.put(i, tmpList);
                }
            });
        }
        pullGraph.shutdown();
        try {
            pullGraph.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
        }
        return graph;
    }


    private Map<String, List<String>> reverseGraph(final Map<String, String> urlContent, int threadNumber) {
        final Map<String, List<String>> graph = new HashMap<String, List<String>>();


        for (String i : urlContent.keySet()) {
            graph.put(i, new ArrayList());
        }
        ExecutorService pullReverseGraph = Executors.newFixedThreadPool(threadNumber);

        for (final String j : graph.keySet()) {
            final String tmpIteratorUrl = j;
            pullReverseGraph.submit(new Runnable() {
                public void run() {


                    List<String> tmpList = new ArrayList<String>();
                    tmpList.clear();

                    for (String i : graph.keySet()) {

                        if (urlContent.get(j).contains(i) && (i != j)) {
                            tmpList.add(i);
                        }
                    }
                    graph.put(j, tmpList);
                }
            });
        }
        return graph;
    }

    private Map<String, Double> getRanks(Map<String, String> urlContent, Map<String, List<String>> graph) {
        Map<String, Double> ranks = new HashMap();
        double complexDampingFactor = 1.0 / graph.size();
        double startingDampingFactor = (1.0 - dampingFactor) / graph.size();

        for (String i : graph.keySet()) {
            ranks.put(i, complexDampingFactor);
        }

        double tmpRank = 0.0;
        for (int k = 0; k < numberOfLoopsInRankComputing; k++) {
            for (String i : graph.keySet()) {
                tmpRank = startingDampingFactor;
                for (String j : graph.keySet()) {
                    if ((graph.get(j).contains(i)) && !(i.equals(j))) {
                        tmpRank += dampingFactor * (ranks.get(j) / graph.get(j).size());
                    }
                }
                ranks.put(i, tmpRank);
            }
        }
        return ranks;
    }


}