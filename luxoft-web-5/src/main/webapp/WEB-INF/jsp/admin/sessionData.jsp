<%@ page import="com.luxoft.cources.dnepr.unit17.utils.Constants" %>


<!DOCTYPE html>
<html>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<head>
    <title>user</title>
    <meta charset="UTF-8"/>
</head>
<body>
<TABLE Border>
    <tr>
        <td>Active Session</td>
        <td>
            <div id="activeSession"><%= (Constants.CONT_USER_SESSION.get() + Constants.CONT_ADMIN_SESSION.get()) %>
            </div>
        </td>
    </tr>
    <tr>
        <td>Active Session(ROLE User)</td>
        <td>
            <div id="asUser"><%=(Constants.CONT_USER_SESSION.get())%>
            </div>
        </td>
    </tr>
    <tr>
        <td>Active Session(ROLE Admin)</td>
        <td>
            <div id="asAdmin"><%=(Constants.CONT_ADMIN_SESSION.get())%>
            </div>
        </td>
    </tr>
    <tr>
        <td>Total Count of HTTP Requests</td>
        <td>
            <div id="tcHttp"><%=(Constants.TOTAL_COUNT_GET_REQEST.get() + Constants.TOTAL_COUNT_POST_REQEST.get() + Constants.TOTAL_COUNT_OTHER_REQEST.get())%>
            </div>
        </td>
    </tr>
    <tr>
        <td>Total Count of POST Requests</td>
        <td>
            <div id="tcPost"><%=Constants.TOTAL_COUNT_POST_REQEST.get()%>
            </div>
        </td>
    </tr>
    <tr>
        <td>Total Count of GET Requests</td>
        <td>
            <div id="tcGet"><%=Constants.TOTAL_COUNT_GET_REQEST.get()%>
            </div>
        </td>
    </tr>
    <tr>
        <td>Total Count of Other Requests</td>
        <td>
            <div id="tcOther"><%=Constants.TOTAL_COUNT_OTHER_REQEST.get()%>
            </div>
        </td>
    </tr>
</TABLE>
<p align="right"><a href=<%=request.getContextPath()+Constants.LOGOUT%>>Logout</a></p>
</body>
</html>
