<%
    String invisibleTeg = "Wrong login or password";
    if (request.getParameter("error") == null) {
        invisibleTeg = " ";
    }
%>

<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="css/sizer.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>
<body>
<div id="formLoginPass">
    <form action="/jspUnit17/login" method="get">
        Login: <input type="text" name="login" value="user1"> <br>
        Password: <input type="text" name="password" value="pswd1"> <br>
        <input type="submit" value="Log in"></form>
</div>

<div id="fatalErorr">
    <span style="color:red">
    <%=invisibleTeg%></span>
</div>
</body>
</html>