package com.luxoft.cources.dnepr.unit17.model;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 16.06.13
 * Time: 20:31
 * To change this template use File | Settings | File Templates.
 */
public class Client {
    private String login;
    private String password;
    private String role;

    public String getRole() {
        return role;
    }

    public String getPassword() {
        return password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(String role) {
        this.role = role;
    }


}
