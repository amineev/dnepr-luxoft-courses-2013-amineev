package com.luxoft.cources.dnepr.unit17.servlets;

import com.luxoft.cources.dnepr.unit17.utils.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 19.06.13
 * Time: 15:06
 * To change this template use File | Settings | File Templates.
 */
public class ServletLogOut extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if ("admin".equals(req.getSession(false).getAttribute("role"))) {
            Constants.CONT_ADMIN_SESSION.decrementAndGet();
        }
        if ("user".equals(req.getSession(false).getAttribute("role"))) {
            Constants.CONT_USER_SESSION.decrementAndGet();
        }
        if (req.getSession(false) != null) {
            req.getSession(false).invalidate();
        }
        resp.sendRedirect(getServletContext().getContextPath() + Constants.MAINPAGE);
        return;
    }
}
