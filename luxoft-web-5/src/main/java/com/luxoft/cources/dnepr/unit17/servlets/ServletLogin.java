package com.luxoft.cources.dnepr.unit17.servlets;


import com.luxoft.cources.dnepr.unit17.utils.ClientUtils;
import com.luxoft.cources.dnepr.unit17.utils.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 15.06.13
 * Time: 17:31
 * To change this template use File | Settings | File Templates.
 */
public class ServletLogin extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String role = null;
        String login = null;
        String pass = null;
        login = request.getParameter("login");
        pass = request.getParameter("password");
        role = ClientUtils.ClientRole(login, pass);
        if (role == null) {
            response.sendRedirect(getServletContext().getContextPath() + Constants.MAINPAGE_ERR);
            return;
        }
        HttpSession session = request.getSession(true);
        session.setAttribute("login", login);
        session.setAttribute("role", role);
        if ("user".equals(role)) {
            Constants.CONT_USER_SESSION.incrementAndGet();
            response.sendRedirect(getServletContext().getContextPath() + Constants.USER);
            return;
        }
        if ("admin".equals(role)) {
            Constants.CONT_ADMIN_SESSION.incrementAndGet();
            response.sendRedirect(getServletContext().getContextPath() + Constants.ADMIN);
            return;
        }
    }
}
