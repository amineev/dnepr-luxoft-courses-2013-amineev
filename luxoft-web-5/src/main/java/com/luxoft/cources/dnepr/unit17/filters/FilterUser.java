package com.luxoft.cources.dnepr.unit17.filters;

import com.luxoft.cources.dnepr.unit17.utils.Constants;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FilterUser implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        if (request.getSession().getAttribute("login") == null) {
            response.sendRedirect(request.getContextPath() + Constants.MAINPAGE);
            return;
        }
        String tmp = (String) request.getSession().getAttribute("role");
        if (!(("admin".equals(tmp) || "user".equals(tmp)))) {
            response.sendRedirect(request.getContextPath() + Constants.MAINPAGE);
            return;
        }
        filterChain.doFilter(request, response);
        return;
    }

    public void destroy() {
    }
}