package com.luxoft.cources.dnepr.unit17.liseners;

import com.luxoft.cources.dnepr.unit17.utils.ClientUtils;
import com.luxoft.cources.dnepr.unit17.utils.Constants;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.concurrent.atomic.AtomicLong;

public class ApplicationContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ClientUtils.fillMainMap(sce.getServletContext().getInitParameter("users"));
        sce.getServletContext().setAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE, new AtomicLong(0));
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        sce.getServletContext().removeAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE);
    }
}
