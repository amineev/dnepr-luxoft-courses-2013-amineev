package com.luxoft.cources.dnepr.unit17.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.luxoft.cources.dnepr.unit17.model.Client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 24.06.13
 * Time: 15:59
 * To change this template use File | Settings | File Templates.
 */
public class ClientUtils {
    private static Map<String, Client> clientsInfoMap = new HashMap<String, Client>();
    private static boolean flagOfFirstLoadMap = true;

    public static void fillMainMap(String s) {
        if (flagOfFirstLoadMap) {
            Gson gson = new Gson();
            ArrayList<Client> lst = new ArrayList<Client>();
            lst = gson.fromJson(s, new TypeToken<ArrayList<Client>>() {
            }.getType());
            for (Client i : lst) {
                clientsInfoMap.put(i.getLogin(), i);
            }
            flagOfFirstLoadMap = false;
        }
    }

    public static String ClientRole(String login, String password) {
        Client client = clientsInfoMap.get(login);
        if (client == null) {
            return null;
        }
        if (client.getPassword().equals(password)) {
            if (client.getRole().equals("admin")) {
                return "admin";
            }
            if (client.getRole().equals("user")) {
                return "user";
            }
        }
        return null;
    }


}
