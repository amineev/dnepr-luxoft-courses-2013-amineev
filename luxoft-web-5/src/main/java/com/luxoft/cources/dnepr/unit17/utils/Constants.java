package com.luxoft.cources.dnepr.unit17.utils;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 24.06.13
 * Time: 15:48
 * To change this template use File | Settings | File Templates.
 */
public interface Constants {
    public static final String MAINPAGE = "/index";
    public static final String DEEP_IN = "/WEB-INF/jsp";
    public static final String MAINPAGE_ERR = MAINPAGE + "?error=1";
    public static final String LOGOUT = "/logout";
    public static final String ADMIN = "/admin/sessionData";
    public static final String USER = "/user";
    String ACTIVE_SESSION_ATTRIBUTE = "ACTIVE_SESSION";
    String ACTIVE_SESSION_MESSAGE = "Active session count is %s";
    public static AtomicInteger CONT_USER_SESSION = new AtomicInteger(0);
    public static AtomicInteger CONT_ADMIN_SESSION = new AtomicInteger(0);
    public static AtomicInteger TOTAL_COUNT_ALL_REQEST = new AtomicInteger(0);
    public static AtomicInteger TOTAL_COUNT_POST_REQEST = new AtomicInteger(0);
    public static AtomicInteger TOTAL_COUNT_GET_REQEST = new AtomicInteger(0);
    public static AtomicInteger TOTAL_COUNT_OTHER_REQEST = new AtomicInteger(0);


}


