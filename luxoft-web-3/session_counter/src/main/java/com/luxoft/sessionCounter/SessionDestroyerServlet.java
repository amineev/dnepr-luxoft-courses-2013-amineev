package com.luxoft.sessionCounter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

public class SessionDestroyerServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession(false) != null) {
            req.getSession(false).invalidate();
        }
        resp.getWriter().println(String.format(Constants.ACTIVE_SESSION_MESSAGE
                , ((AtomicLong) getServletContext().getAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE)).get()));
    }

}
