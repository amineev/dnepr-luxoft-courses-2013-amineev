package com.luxoft.dnepr.courses.unit15.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 19.06.13
 * Time: 15:45
 * To change this template use File | Settings | File Templates.
 */
public class User extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        HttpSession session = req.getSession();
        String name = (String) session.getAttribute("login");
        StringBuffer sb = new StringBuffer();
        sb.append("\n" +
                "<!DOCTYPE html>\n" +
                "<html> \n" +
                "<head>  \n" +
                "    <title>user</title>  \n" +
                "    <meta charset=\"UTF-8\"/>\n" +
                "</head>  <body>\n" +
                "<p align=\"right\"><a href=\"/firstWebAppUnit15/logout\">logout </a></p>\n" +
                "<br>\n" +
                "<br><br><br><br><h1>\n" +
                "    <p align=\"center\">Hello " + name + "</p>\n" +
                "     </h1>\n" +
                " </body>\n" +
                "</html>");

        writer.println(sb);
    }

}
