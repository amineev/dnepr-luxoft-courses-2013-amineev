package com.luxoft.dnepr.courses.unit15.model;

import java.util.concurrent.atomic.AtomicInteger;

public class Constans {
    public static final String MAINPAGE = "/firstWebAppUnit15/index.html";
    public static final String ADMIN = "/firstWebAppUnit15/admin/sessionData";
    public static final String USER = "/firstWebAppUnit15/user";
    public static AtomicInteger CONT_USER_SESSION = new AtomicInteger(0);
    public static AtomicInteger CONT_ADMIN_SESSION = new AtomicInteger(0);
    public static AtomicInteger TOTAL_COUNT_ALL_REQEST = new AtomicInteger(0);
    public static AtomicInteger TOTAL_COUNT_POST_REQEST = new AtomicInteger(0);
    public static AtomicInteger TOTAL_COUNT_GET_REQEST = new AtomicInteger(0);
    public static AtomicInteger TOTAL_COUNT_OTHER_REQEST = new AtomicInteger(0);
}
