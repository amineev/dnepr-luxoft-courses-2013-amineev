package com.luxoft.dnepr.courses.unit15.servlets;

import com.luxoft.dnepr.courses.unit15.model.Constans;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 16.06.13
 * Time: 21:33
 * To change this template use File | Settings | File Templates.
 */
public class Admin extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if ("user".equals(req.getSession().getAttribute("role"))) {
            resp.sendRedirect(Constans.USER);
        }
        ;
        PrintWriter writer = resp.getWriter();

        StringBuffer sb = new StringBuffer(" ");
        sb.append("<!DOCTYPE html>\n" +
                " <html>\n" +
                "<title></title>\n" +
                "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +
                "<head>\n" +
                "<title>user</title>\n" +
                "<meta charset=\"UTF-8\"/>\n" +
                "</head>  <body>\n" +
                "<TABLE Border>\n" +
                "<tr>\n" +
                "<td>Active Session</td>\n" +
                "<td><div id=\"activeSession\">" + (Constans.CONT_USER_SESSION.get() + Constans.CONT_ADMIN_SESSION.get()) + "</div></td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td>Active Session(ROLE User)</td>\n" +
                "<td><div id=\"asUser\">" + (Constans.CONT_USER_SESSION.get()) + "</div></td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td>Active Session(ROLE Admin)</td>\n" +
                "<td><div id=\"asAdmin\">" + (Constans.CONT_ADMIN_SESSION.get()) + "</div></td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td>Total Count of HTTP Requests</td>\n" +
                "<td><div id=\"tcHttp\">" + (Constans.TOTAL_COUNT_GET_REQEST.get() + Constans.TOTAL_COUNT_POST_REQEST.get() + Constans.TOTAL_COUNT_OTHER_REQEST.get()) + "</div></td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td>Total Count of POST Requests</td>\n" +
                "<td><div id=\"tcPost\">" + Constans.TOTAL_COUNT_POST_REQEST.get() + "</div></td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td>Total Count of GET Requests</td>\n" +
                "<td><div id=\"tcGet\">" + Constans.TOTAL_COUNT_GET_REQEST.get() + "</div></td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td>Total Count of Other Requests</td>\n" +
                "<td><div id=\"tcOther\">" + Constans.TOTAL_COUNT_OTHER_REQEST.get() + "</div></td>\n" +
                "</tr>\n" +
                "</TABLE>\n" +
                "<p align=\"right\"><a href=\"/firstWebAppUnit15/logout\">logout </a></p>\n" +
                " </body>\n" +
                "</html>");


        writer.println(sb);


    }
}
