package com.luxoft.dnepr.courses.unit15.servlets;


import com.luxoft.dnepr.courses.unit15.model.Client;
import com.luxoft.dnepr.courses.unit15.model.Constans;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 15.06.13
 * Time: 17:31
 * To change this template use File | Settings | File Templates.
 */
public class Login extends HttpServlet {
    public String takeJson() {
        String json = getServletContext().getInitParameter("users");
        return json;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {


        String role = null;
        String login = null;
        String pass = null;


        login = request.getParameter("login");
        pass = request.getParameter("password");

        Client client = Client.getClient(takeJson(), login);
        if ((client == null) || (!pass.equals(client.getPassword()))) {

            response.sendRedirect(Constans.MAINPAGE + "?error=1");
            return;
        }

        HttpSession session = request.getSession(true);
        if (session == null) {
            response.sendRedirect(Constans.MAINPAGE);
            return;
        }

        session.setAttribute("login", client.getLogin());
        session.setAttribute("role", client.getRole());
        role = (String) session.getAttribute("role");

        if ("user".equals(role)) {
            Constans.CONT_USER_SESSION.incrementAndGet();
            response.sendRedirect(Constans.USER);
            return;
        }
        if ("admin".equals(role)) {
            Constans.CONT_ADMIN_SESSION.incrementAndGet();
            response.sendRedirect("/firstWebAppUnit15/admin/sessionData");
            //response.sendRedirect(Constans.ADMIN);
            return;
        }
    }


}
