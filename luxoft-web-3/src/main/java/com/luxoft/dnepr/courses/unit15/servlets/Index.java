package com.luxoft.dnepr.courses.unit15.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 16.06.13
 * Time: 15:57
 * To change this template use File | Settings | File Templates.
 */
public class Index extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        String invisibleTeg = "Wrong login or password";
        if (req.getParameter("error") == null) {
            invisibleTeg = " ";


        }


        StringBuffer sb = new StringBuffer();
        sb.append("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "<title></title>\n" +
                "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +
                "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/sizer.css\" />\n" +
                "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/style.css\" />\n" +
                "\n" +
                "    <script type=\"text/javascript\" src=\"js/jquary.js\" ></script>\n" +
                "    <script type=\"text/javascript\" src=\"js/jquary_cookie.js\" ></script>\n" +
                "\n" +
                "    <script type=\"text/javascript\" >\n" +
                "    $(document).ready(function() {\n" +
                "    if ($.cookie(\"notFirstTry\") == 1) {\n" +
                "    $(\"#fatalErorr\").css(\"display\",\"block\");\n" +
                "    }\n" +
                "    });\n" +
                "    </script>\n" +
                "  </head>\n" +
                "<body>\n" +
                "<div id=\"formLoginPass\">\n" +
                "<form action=\"/firstWebAppUnit15/login\" method=\"get\">\n" +
                "Login: <input  type=\"text\" name=\"login\" value=\"user1\"> <br>\n" +
                "Password: <input  type=\"text\"name=\"password\" value=\"pswd1\"> <br>\n" +
                "<input type=\"submit\"value=\"Log in\" ></form>\n" +
                "</div>\n" +
                "\n" +
                "<div id =\"fatalErorr\">\n" +
                "<span style=\"color:red\">"+
                invisibleTeg +
                "</span>"+
                "</div>\n" +
                "</body>\n" +
                "</html>");
        writer.println(sb);

    }
}
