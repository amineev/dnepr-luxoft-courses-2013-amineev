package com.luxoft.courses.firstWebApp;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 09.06.13
 * Time: 18:52
 * To change this template use File | Settings | File Templates.
 */
public class PutDummy extends HttpServlet {
    private static Map<String, Integer> dataBase = new ConcurrentHashMap<String, Integer>();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        String name = null;
        String age = null;
        name = request.getParameter("name");
        age = request.getParameter("age");
        if ((name == null) || (age == null)) {
            response.setStatus(500);
            response.getHeader("Internal Server Error");
            writer.println("\"error\": \"Illegal parameters\"");
            response.setContentType("application/json; charset=utf-8");
        } else if (dataBase.containsKey(name)) {
            response.setStatus(500);
            writer.println("\"error\":\"Name " + name + " already exists");
            response.setContentType("application/json; charset=utf-8");

        } else {
            dataBase.put(name, Integer.parseInt(age));
            response.setStatus(201);
        }


    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter writer = response.getWriter();
        String name = null;
        String age = null;
        name = request.getParameter("name");
        age = request.getParameter("age");
        if ((name == null) || (age == null)) {
            response.setStatus(500);
            response.getHeader("Internal Server Error");
            writer.println("\"error\": \"Illegal parameters\"");
            response.setContentType("application/json; charset=utf-8");
        } else if (!(dataBase.containsKey(name))) {
            response.setStatus(500);
            writer.println("\"error\":\"Name " + name + "  does not exist");
            response.setContentType("application/json; charset=utf-8");

        } else {
            dataBase.put(name, Integer.parseInt(age));
            response.setStatus(202);
        }

    }
}
