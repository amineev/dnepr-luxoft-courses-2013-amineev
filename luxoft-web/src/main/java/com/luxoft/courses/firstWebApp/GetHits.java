package com.luxoft.courses.firstWebApp;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 09.06.13
 * Time: 18:49
 * To change this template use File | Settings | File Templates.
 */
public class GetHits extends HttpServlet {
    private static Integer hiters = 0;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        synchronized (hiters) {
            hiters++;
        }
        String strTmp = "\"hitCount\"";
        writer.println(hiters.toString());
        response.setStatus(200);
        response.setContentType("application/json; charset=utf-8");
        strTmp += hiters.toString();
        response.setContentLength(strTmp.length());

    }

}


