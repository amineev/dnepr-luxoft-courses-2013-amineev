package com.luxoft.courses.unit14;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 15.06.13
 * Time: 17:31
 * To change this template use File | Settings | File Templates.
 */
public class Login extends HttpServlet {
    private boolean pasLoginChecker(String pas, String login) {
        Gson gson = new Gson();
        String json = getServletContext().getInitParameter("pasLogins");

        Map<String, String> hm = new ConcurrentHashMap<String, String>();
        hm = gson.fromJson(json, new TypeToken<Map<String, String>>() {
        }.getType());
        if (pas.equals(hm.get(login))) {
            return true;
        }
        return false;
    }

    ;


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        PrintWriter writer = response.getWriter();
        String login = null;
        String pass = null;
        login = request.getParameter("login");
        pass = request.getParameter("password");

        if (!(login == null) && !(pass == null)) {

            if (pasLoginChecker(pass, login)) {

                Cookie cookieTrue = new Cookie("login", login);
                response.addCookie(cookieTrue);

                response.addCookie(new Cookie("notFirstTry", "0"));
                response.sendRedirect("/myFirstWebApp/user");
                return;
            } else {

                Cookie cookieFalse = new Cookie("notFirstTry", "1");
                cookieFalse.setMaxAge(100);
                response.addCookie(cookieFalse);
                response.sendRedirect("/myFirstWebApp/index.html");
                return;
            }
        }

    }


}
