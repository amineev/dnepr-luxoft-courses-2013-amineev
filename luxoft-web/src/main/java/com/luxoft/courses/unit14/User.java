package com.luxoft.courses.unit14;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 15.06.13
 * Time: 18:33
 * To change this template use File | Settings | File Templates.
 */
public class User extends HttpServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        Cookie[] cookies = req.getCookies();
        String name = "";
        for (Cookie i : cookies) {
            if (i.getName().equals("login")) {
                name = i.getValue();
                break;
            }
        }
        StringBuffer sb = new StringBuffer(" ");
        sb.append("\n" +
                "<!DOCTYPE html>\n" +
                "<html> \n" +
                "<head>  \n" +
                "    <title>user</title>  \n" +
                "    <meta charset=\"UTF-8\"/>\n" +
                "</head>  <body>\n" +
                "<p align=\"right\"><a href=\"/myFirstWebApp/index.html\">logout </a></p>\n" +
                "<br>\n" +
                "<br><br><br><br><h1>\n" +
                "    <p align=\"center\">Hello " + name + "</p>\n" +
                "     </h1>\n" +
                " </body>\n" +
                "</html>");
        writer.println(sb);


    }
}
