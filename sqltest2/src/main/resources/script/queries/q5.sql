SELECT pc.model, pc.price
FROM  pc
LEFT JOIN product ON pc.model = product.model
LEFT JOIN makers ON product.maker_id = makers.maker_id
WHERE makers.maker_name = 'B'

UNION ALL

SELECT laptop.model, laptop.price
FROM  laptop
LEFT JOIN product ON laptop.model = product.model
LEFT JOIN makers ON product.maker_id = makers.maker_id
WHERE makers.maker_name = 'B'

UNION ALL

SELECT printer.model, printer.price
FROM  printer
LEFT JOIN product ON printer.model = product.model
LEFT JOIN makers ON product.maker_id = makers.maker_id
WHERE makers.maker_name = 'B'
