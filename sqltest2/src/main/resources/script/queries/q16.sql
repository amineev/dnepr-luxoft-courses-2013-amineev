SELECT makers.maker_id, COUNT(makers.maker_id) AS mod_count
FROM makers
INNER JOIN product ON makers.maker_id = product.maker_id
INNER JOIN pc ON pc.model = product.model
GROUP BY maker_id
HAVING mod_count > 3