SELECT AVG(hd)
FROM pc
INNER JOIN product ON pc.model = product.model
INNER JOIN makers  ON makers.maker_id = product.maker_id
WHERE makers.maker_id IN (
  SELECT makers.maker_id
  FROM makers
  INNER JOIN product ON makers.maker_id = product.maker_id
  INNER JOIN printer ON printer.model = product.model)