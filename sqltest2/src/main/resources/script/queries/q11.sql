SELECT tmp.maker_id , tmp.type
FROM (
	SELECT COUNT(type) AS product_count, type, maker_id
	FROM product
	GROUP BY type, maker_id
) AS tmp
WHERE tmp.product_count >= 2