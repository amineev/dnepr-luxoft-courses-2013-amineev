SELECT makers.maker_id, AVG(pc.hd) AS avg_hd
FROM makers
INNER JOIN product ON product.maker_id = makers.maker_id
INNER JOIN pc ON pc.model = product.model
WHERE makers.maker_id IN (
	SELECT makers.maker_id
	FROM makers
	INNER JOIN product ON product.maker_id = makers.maker_id
	INNER JOIN printer ON printer.model = product.model
	GROUP BY makers.maker_id
)
GROUP BY makers.maker_id