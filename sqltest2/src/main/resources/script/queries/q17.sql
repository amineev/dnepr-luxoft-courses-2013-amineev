SELECT maker_id
FROM (
  SELECT  product.maker_id, speed, ram
  FROM pc, product
  WHERE product.model = pc.model
  AND product.maker_id
  IN (
    SELECT maker_id
    FROM product
    WHERE type =  'Printer')
) AS temp
WHERE ram = (
  SELECT MIN(ram)
  FROM pc)
GROUP BY maker_id;