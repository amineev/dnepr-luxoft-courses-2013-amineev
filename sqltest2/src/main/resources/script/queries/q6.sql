SELECT DISTINCT makers.maker_name
FROM makers
INNER JOIN product ON makers.maker_id = product.maker_id
INNER JOIN pc ON pc.model = product.model
WHERE makers.maker_id NOT IN (
  SELECT DISTINCT makers.maker_id
  FROM  makers
  INNER JOIN product ON makers.maker_id = product.maker_id
  INNER JOIN laptop ON laptop.model = product.model
)