SELECT product.type, laptop.model, laptop.speed
FROM laptop
LEFT JOIN product ON laptop.model = product.model
WHERE laptop.speed < (SELECT MIN(speed) FROM pc)
