<%@ page import="com.luxoft.dnepr.courses.unit15.model.Constans" %>
<%
    if ("user".equals(request.getSession().getAttribute("role"))) {
        response.sendRedirect(Constans.USER);
    }

%>
<!DOCTYPE html>
<html>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<head>
    <title>user</title>
    <meta charset="UTF-8"/>
</head>
<body>
<TABLE Border>
    <tr>
        <td>Active Session</td>
        <td>
            <div id="activeSession"><%= (Constans.CONT_USER_SESSION.get() + Constans.CONT_ADMIN_SESSION.get()) %>
            </div>
        </td>
    </tr>
    <tr>
        <td>Active Session(ROLE User)</td>
        <td>
            <div id="asUser"><%=(Constans.CONT_USER_SESSION.get())%>
            </div>
        </td>
    </tr>
    <tr>
        <td>Active Session(ROLE Admin)</td>
        <td>
            <div id="asAdmin"><%=(Constans.CONT_ADMIN_SESSION.get())%>
            </div>
        </td>
    </tr>
    <tr>
        <td>Total Count of HTTP Requests</td>
        <td>
            <div id="tcHttp"><%=(Constans.TOTAL_COUNT_GET_REQEST.get() + Constans.TOTAL_COUNT_POST_REQEST.get() + Constans.TOTAL_COUNT_OTHER_REQEST.get())%>
            </div>
        </td>
    </tr>
    <tr>
        <td>Total Count of POST Requests</td>
        <td>
            <div id="tcPost"><%=Constans.TOTAL_COUNT_POST_REQEST.get()%>
            </div>
        </td>
    </tr>
    <tr>
        <td>Total Count of GET Requests</td>
        <td>
            <div id="tcGet"><%=Constans.TOTAL_COUNT_GET_REQEST.get()%>
            </div>
        </td>
    </tr>
    <tr>
        <td>Total Count of Other Requests</td>
        <td>
            <div id="tcOther"><%=Constans.TOTAL_COUNT_OTHER_REQEST.get()%>
            </div>
        </td>
    </tr>
</TABLE>
<p align="right"><a href="/jspUnit16/logout">logout </a></p>
</body>
</html>
