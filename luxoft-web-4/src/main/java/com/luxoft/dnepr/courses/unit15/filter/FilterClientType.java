package com.luxoft.dnepr.courses.unit15.filter;

import com.luxoft.dnepr.courses.unit15.model.Client;
import com.luxoft.dnepr.courses.unit15.model.Constans;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FilterClientType implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;


        if (request.getSession().getAttribute("login") == null) {
            response.sendRedirect(Constans.MAINPAGE);
            return;
        }
        Client client = Client.getClient(request.getServletContext().getInitParameter("users"), request.getSession().getAttribute("login").toString());
        if (client != null) {
            filterChain.doFilter(request, response);
            return;
        }
    }

    public void destroy() {
    }
}