package com.luxoft.dnepr.courses.unit15.lisener;

public interface Constants {
    String ACTIVE_SESSION_ATTRIBUTE = "ACTIVE_SESSION";
    String ACTIVE_SESSION_MESSAGE = "Active session count is %s";
}
