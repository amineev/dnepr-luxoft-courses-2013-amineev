package com.luxoft.dnepr.courses.unit15.lisener;

import com.luxoft.dnepr.courses.unit15.model.Constans;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 19.06.13
 * Time: 16:49
 * To change this template use File | Settings | File Templates.
 */
public class HttpCounter implements ServletRequestListener {


    public void requestDestroyed(ServletRequestEvent sre) {
    }

    public void requestInitialized(ServletRequestEvent sre) {

        if (sre.getServletRequest() instanceof HttpServletRequest) {
            if (selectRequest(sre, "POST")) {
                Constans.TOTAL_COUNT_POST_REQEST.getAndIncrement();
            } else if (selectRequest(sre, "GET")) {
                Constans.TOTAL_COUNT_GET_REQEST.getAndIncrement();
            } else {
                Constans.TOTAL_COUNT_OTHER_REQEST.getAndIncrement();
            }
        }

    }

    private boolean selectRequest(ServletRequestEvent sre, String requestType) {
        return ((HttpServletRequest) sre.getServletRequest()).getMethod().equals(requestType);
    }
}
