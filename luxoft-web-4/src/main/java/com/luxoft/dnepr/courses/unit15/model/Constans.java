package com.luxoft.dnepr.courses.unit15.model;

import java.util.concurrent.atomic.AtomicInteger;

public class Constans {
    public static final String MAINPAGE = "/jspUnit16/index.jsp";
    public static final String ADMIN = "/jspUnit16/admin/sessionData";
    public static final String USER = "/jspUnit16/user";
    public static AtomicInteger CONT_USER_SESSION = new AtomicInteger(0);
    public static AtomicInteger CONT_ADMIN_SESSION = new AtomicInteger(0);
    public static AtomicInteger TOTAL_COUNT_ALL_REQEST = new AtomicInteger(0);
    public static AtomicInteger TOTAL_COUNT_POST_REQEST = new AtomicInteger(0);
    public static AtomicInteger TOTAL_COUNT_GET_REQEST = new AtomicInteger(0);
    public static AtomicInteger TOTAL_COUNT_OTHER_REQEST = new AtomicInteger(0);
}
